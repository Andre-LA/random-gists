/*
How to build this file:

- Build cyber as an WASM library:
https://github.com/fubark/cyber/blob/master/docs/build.md#build-as-a-library

- Download and setup WASI SDK:
https://github.com/WebAssembly/wasi-sdk#install

- use llvm-ar from WASI to make an archive of wasm cyber object (WASI_SDK_PATH refers to the path where WASI it's setup)
WASI_SDK_PATH/bin/llvm-ar -rcs libcyber.a zig-out/lib/cyber.wasm.o

- use clang from WASI to compile this code, linking it with cyber archive
WASI_SDK_PATH/bin/clang main.c -o main.wasm -lcyber -L.

- we have our wasm executable, run it with wasmtime
wasmtime main.wasm

output:
Hello C world!
hello from Cyber����
--> 10
--> 30
40
Sucess!
*/

#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include "cyber.h"

// host functions
void hostFetchUrl(const char *url, size_t urlLen) {}
void hostEvalJS(const char *ptr, size_t len) {}
void hostSleep(uint64_t secs, uint64_t nsecs) {}
void hostLogDebug(const char *ptr, size_t len) {}
void hostLogInfo(const char *ptr, size_t len) {}
void hostLogWarn(const char *ptr, size_t len) {}
void hostLogError(const char *ptr, size_t len) {}
double hostMilliTimestamp() {
    return 0.016;
}
void hostFileWrite(uint32_t fid, const char* str, size_t strLen) {
    printf("%s", str);
}

// our function to be added to Cyber

int add(int a, int b) {
    return a + b;
}

CyValue add_wrapper(CyUserVM *vm, CyValue *args, uint8_t nargs) {
    double left = cyValueAsNumber(args[0]);
    double right = cyValueAsNumber(args[1]);
    printf("--> %d\n", (int)left);
    printf("--> %d\n", (int)right);
    return cyValueInteger(add((int)left, (int)right));
}

// load the core, binding the `add` function

bool loadCore(CyUserVM *vm, CyModule *mod) {
    cyVmSetModuleFunc(vm, mod, cstr("add"), 2, add_wrapper);
    return true;
}

// our program :)
int main(void) {
    printf("Hello C world!\n");
    
    CyUserVM* vm = cyVmCreate();

    cyVmAddModuleLoader(vm, cstr("core"), loadCore);
  
    CStr src = cstr(
        "print 'hello from Cyber'\n"
        "v = add(10, 30)\n"
        "print v\n"
        "\n"
    );

    CyValue val;
    int res = cyVmEval(vm, src, &val);

    if (res == CY_Success) {
        printf("Sucess!\n");
        cyVmRelease(vm, val);
    } else {
        CStr err = cyVmGetLastErrorReport(vm);
        printf("%s\n", err.charz);
    }
  
    cyVmDestroy(vm);
    return 0;
}
