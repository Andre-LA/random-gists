-- this implements function overloading like in Odin:
-- https://odin-lang.org/docs/overview/#explicit-procedure-overloading
-- note: this could be improved type checking and all, but I decided to make it simple instead.

local function_overloading = {}

function function_overloading.match(overloaded_functions, ...)
  for _, overloaded_fn in ipairs(overloaded_functions) do
    local matches = 0

    for argtype_n, argtype in ipairs(overloaded_fn.type.argtypes) do
      if argtype == select(argtype_n, ...).type then
        matches = matches + 1
      end
    end

    if matches == select('#', ...) then
      return overloaded_fn
    end
  end
end

return function_overloading
