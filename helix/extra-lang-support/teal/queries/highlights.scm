;;; Highlighting for teal

;; Builtins
((identifier) @variable.builtin
 (#eq? @variable.builtin "self"))

;; Keywords
(if_statement
[
  "if"
  "then"
  "end"
] @keyword.control.conditional)

(elseif_block
[
  "elseif"
  "then"
  "end"
] @keyword.control.conditional)

(else_block
[
  "else"
  "end"
] @keyword.control.conditional)

(numeric_for_statement
[
  "for"
  "do"
  "end"
] @keyword.control.repeat)

(while_statement
[
  "while"
  "do"
  "end"
] @keyword.control.repeat)

(repeat_statement
[
  "repeat"
  "until"
] @keyword.control.repeat)

(do_statement
[
  "do"
  "end"
] @keyword)

"return" @keyword.control.return

[
  "in"
  "local"
  "global"
  (break)
  "goto"
  "record"
  "enum"
  "type"
] @keyword

(function_statement "function" @keyword.function . name: (_) @function)
(function_body "end" @keyword.function)

(anon_function
[
  "function"
  "end"
] @keyword.function)

;; Operators
;; [
;;  ; "not"
;;  ; "and"
;;  ; "or"
;;  ; "as"
;;  ; "is"
;; ] @keyword.operator

; [
 ; "="
 ; "~="
 ; "=="
 ; "<="
 ; ">="
 ; "<"
 ; ">"
 ; "+"
 ; "-"
 ; "%"
 ; "/"
 ; "//"
 ; "*"
 ; "^"
 ; "&"
 ; "~"
 ; "|"
 ; ">>"
 ; "<<"
 ; ".."
 ; "#"
; ] @operator


;; Punctuation
["," "." ":" ";"] @punctuation.delimiter

;; Brackets
[
 "("
 ")"
 "["
 "]"
 "{"
 "}"
] @punctuation.bracket

;; Constants
(boolean) @constant.builtin.boolean
(nil) @constant.builtin
(varargs) @constant

((identifier) @constant
 (#match? @constant "^[A-Z][A-Z_0-9]*$"))

;; Parameters
(arguments
  (identifier) @variable.parameter)

;; Functions
(function_statement name: (identifier) @function)
(function_call called_object: (identifier) @function.call)

;; Nodes
(comment) @comment
(string) @string
(number) @constant.numeric.integer
(label) @label
(shebang_comment) @comment

(index . (identifier) @variable.other.member (_))

;; Variables and attributes
(var_declaration
  declarators: (var_declarators
    (var name: (identifier) @variable)))

(var_declaration
  declarators: (var_declarators
    (var 
      "<" @punctuation.bracket
      attribute: (attribute) @attribute
      ">" @punctuation.bracket)))

;; Error
(ERROR) @error
