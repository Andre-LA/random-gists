// NOTE: I'm not a Pascal user and never really used it, I just know some basic stuff about it.

{-
#include <stdio.h>
int main(void) {
	printf("Hello World!\n");
	return 0;
}
-}

// FEEDBACK: the usage of {- -} for comments is interesting.

import cstdlib.stdio;
fn main(): i32 {
	print("Hello World!\n");
	return 0;
}

// hello world
/* hello world */
{- hello world -}
//---------------------------

// FEEDBACK: I'm not sure if I like "var x const i32" syntax, there are two points:
// first, mixing "var" and "const" it's a bit weird, I would prefer using `const a i32` instead.
// second, function returns uses :, so it's a bit weird not seeing : for types here too.
// That said, that's just a preference.

var a i32;
var a const i32;
var a i32^; 
var a i32!^; // FEEDBACK: I didn't understood the use of !, it's for nullable pointers? 
var a const i32^;
var a const i32!^;
var b i32& = a&;

// FEEDBACK: I like the idea of using ^ and & as postfixes, the & 
// case though I probably would need to get used to.

var pa i32^ = a&;
var a i32 = pa^;
var ppa i32^^ = pa&;

// FEEDBACK: What's the difference between !^ and !& ?

fn sum(s i32&, a i32!&, b i32!&): void {
	s = a + b;
}

// FEEDBACK: that's why I like ^ as postfix, it's easy to understand that a is being
// dereferenced, Zig does the same with "a.*"
fn sum(a vec3^): int {
	return a^.x + a^.y + a^.z;
}

fn swap(a i32^, b i32^): void {
	var t i32 = a^;
	a^ = b^;
	b^ = t;
}



//----------------
var a i32 = 5;
var a i32 = u_;
var b f32 = 2.5;

// FEEDBACK: I wrote earlier that I would prefer "var a i32 / const a i32" syntax, but there's another
// option: using let, like let a i32 = 1, let a const i32 = 2
// here, it would be let len256 constexpr i32 = 256, otherwise constexpr len256 i32 = 256

var len256 constexpr i32 = 256;

// FEEDBACK: Why is "comptime" keyword necessary?
var len_max constexpr i32 = comptime multiply(4, 5);

inline fn sum(a i32, b i32): i32 {}

// FEEDBACK: I think it's better to make it private by default and use
// public (or just pub) as an option,

private fn sum(a i32, b i32): i32 {}

fn sum(a i32, b i32): i32 {
	return a + b;
}

// FEEDBACK: If you don't want to use tuples, multiple returns is the way to go, that said,
// it have it's own quirks, for example, would print(sum_diff(1, 2)) evaluate to print(3, -1)?
// on Lua, which uses multiple returns often, has some tricky semantics about this.
fn sum_diff(a i32, b i32): i32, i32 {
	return a + b, a - b;
}
var s, d = sum_diff(3, 2);


// FEEDBACK: array length as prefix, nice :)
var b [3]i32;
var q [3]i32^; // FEEDBACK: however, is that an array of pointers or a pointer to an array?

int a[_] = [0, 1, 2, 3, 4, 5];
//------------
type angle f32;

// func ptr, fn()
var pf_sum fn(i32): i32, arg i32): i32 = sum&;
type pft_sum fn(i32, i32): i32;
//var fp pft_sum = sum&;
var fp pft_sum;
fp = sum&;

//fn do_twice(opr fun_ptr_t, arg i32): i32 {

// FEEDBACK: IIRC, on Nelua and Teal this is where parameters and multiple returns got ambiguous.
fn do_twice(opr fn(i32): i32, arg i32): i32 {
	return opr(arg) + opr(arg)
}
var answer = do_twice(sum, 5);
